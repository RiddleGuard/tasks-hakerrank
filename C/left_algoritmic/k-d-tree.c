function kdtree (point_t *pointList, int depth){
   // Выбираем ось, основываясь на глубине рекурсии 
 int axis := depth mod k;
      
   // Сортируем список точек относительно выбранной оси и выбираем медиану в качестве опорного элемента (pivot)
   pivot = medianFinder(pointList, axis);
      
   // Создаем узел и строим поддерево
   node.location := pivot; // Копируем координаты опорного элемента
   node.leftChild := kdtree(points в pointList до pivot, depth + 1);
   node.rightChild := kdtree(points в pointList после pivot, depth + 1);
   return node;
