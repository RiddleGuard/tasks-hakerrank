PFA(v):
    for each vertex i
        d[i] = inf
    d[v] = 0
    queue q
    q.push(v)
    while q is not empty
        u = q.front()
        q.pop()
        for each i in adj[u]
            if d[i] > d[u] + w(u,i)
                d[i] = d[u] + w(u,i)
                if i is not in q
                    q.push(i)
