void counting_sort(int* arr, unsigned int len, int min, int max)
{
 int* count = new int[max - min + 1];
 
 for (int i = min; i <= max; ++i)
 count[i - min] = 0;
 
 for (int i = 0; i < len; ++i)
 ++count[arr[i] - min];
 
 for (int i = min; i <= max; ++i)
 for(int j = count[i - min]; j > 0; j--)
 *arr++ = i;
 
 delete[] count;
}
