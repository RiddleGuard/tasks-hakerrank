% https://www.hackerrank.com/challenges/rotate-string/problem
-module(rotate_string).
-export([main/0]).

-define(MIN_COUNT_LINE, 1).
-define(MAX_COUNT_LINE, 10).
-define(MIN_COUNT_CHARS, 1).
-define(MAX_COUNT_CHARS, 100).
-define(SPACE, 32).
-define(CARRIAGE, 10).

main() ->
  {ok, [Count_Line]} = io:fread("","~d"),
  print_output(get_line(Count_Line, [])).

print_output(List) ->
  lists:foreach(fun(L) ->
    io:fwrite("~s~n", [string: join(L, [?SPACE])])
  end, List).

get_line(0, Acc) ->
  lists:reverse(Acc);
get_line(N, Acc)  when ?MIN_COUNT_LINE =< N andalso N =< ?MAX_COUNT_LINE ->
  Line = string: strip(io: get_line(""), right, ?CARRIAGE),
  Len = case length(Line) of
    Count when ?MIN_COUNT_CHARS =< Count
       andalso Count =< ?MAX_COUNT_CHARS -> Count;
    _ -> throw(error)
  end,
  Fmt_List = multi_rotation(Line, Len),
  get_line(N - 1, [Fmt_List | Acc]).

-spec multi_rotation(list(), 0) -> [list()].

multi_rotation(List, N) ->
  multi_rotation(rotation(List), N, []).

multi_rotation(_, 0, Acc) -> lists:reverse(Acc);
multi_rotation(List, N, Acc) ->
  multi_rotation(rotation(List), N - 1, [List | Acc]).

-spec rotation(list()) -> list().

rotation([H | T]) ->
  lists: append(T, [H]).

% ------------------------------------------------------------------------------
-include_lib("eunit/include/eunit.hrl").

forech_list_test() ->
  A = "tron",
  Len = length(A),
  Res = ["ront", "ontr", "ntro", "tron"],
  List = multi_rotation(A, Len),
  ?assertEqual(Res, List).
