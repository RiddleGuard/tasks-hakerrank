-module(dice_path).
-compile([export_all]).
-define(DBG(Z), (fun() -> io:format("~p~n", [Z]), Z end)()).

-export([main/0]).
-define(SPACE, 32).
-define(CARRIAGE, 10).

-define(MIN_LEN_GRID, 1).
-define(MAX_LEN_GRID, 60).
-define(MIN_COUNT_TEST_CASE, 1).
-define(MAX_COUNT_TEST_CASE, 3600).

-record(cube, {
  top    = [1],
  bottom = [6],
  left   = [3],
  right  = [4],
  front  = [2],
  back   = [5]
}).

-record(c, {x = 1, y = 1}).
% -record(grid, {grid = [[#c{}]]}).

main() ->
  [_S | Arr] = read_all(),
  Arr2 = filter_arr(Arr),
  print_output(case_t(Arr2)).

% ------------------------------------------------------------------------------
filter_arr(Arr) -> filter_arr(Arr, []).

filter_arr([], Acc) -> lists:reverse(Acc);
filter_arr([X1, X2 | Tail], Acc) ->
  filter_arr(Tail,
    [#c{x = fun erlang: list_to_integer/1 (X1),
        y = fun erlang: list_to_integer/1 (X1)} | Acc]).

% ------------------------------------------------------------------------------
read_all() ->
    read_all([], io: get_chars("", 4096)).
read_all(In, eof) ->
    string: tokens(lists:flatten(lists: reverse(In)), [?CARRIAGE, ?SPACE]);
read_all(In, Data) ->
    read_all([Data|In], io: get_chars("", 4096)).

% ------------------------------------------------------------------------------
print_output(List) ->
  lists:foreach(fun(Elem) -> io:fwrite("~p ", [Elem]) end, List).

% ------------------------------------------------------------------------------
rotate_right(#cube{
    top    = Top,
    bottom = Bottom,
    left   = Left,
    right  = Right,
    front  = Front,
    back   = Back
}) ->
  #cube{
    top    = [hd(Left)   | Top],
    bottom = [hd(Right)  | Bottom],
    left   = [hd(Bottom) | Left],
    right  = [hd(Top)    | Right],
    front  = [hd(Front)  | Front],
    back   = [hd(Back)   | Back]
  }.

% ------------------------------------------------------------------------------
rotate_down(#cube{
    top    = Top,
    bottom = Bottom,
    left   = Left,
    right  = Right,
    front  = Front,
    back   = Back
}) ->
  #cube{
    top    = [hd(Back)   | Top],
    bottom = [hd(Front)  | Bottom],
    left   = [hd(Left)   | Left],
    right  = [hd(Right)  | Right],
    front  = [hd(Top)    | Front],
    back   = [hd(Bottom) | Back]
  }.

% ------------------------------------------------------------------------------
case_t(Arr) ->
  lists:map(fun (Finish_cordinate) ->
      solve_path(#c{}, Finish_cordinate)
    end, Arr).

% ------------------------------------------------------------------------------
solve_path(Start_cordinate, Finish_cordinate) ->
  Grid = create_empty_arr(Start_cordinate, Finish_cordinate),
  List = solve_path(Start_cordinate, Finish_cordinate, Grid, #cube{}),
  lists: max(?DBG(List)).

solve_path(#c{x = StartX,  y = StartY},
           #c{x = FinishX, y = FinishY}, Grid, Cube) ->
  Cubics =
  element(1,
  mapfoldl_plus(
    fun (List, Next_list, Acc) ->
      {element(2,
      mapfoldl_plus(
        fun (Elem, Next_elem, Cube_cur) when Next_elem =:= none andalso
                                             Next_list =:= none ->
              {finish, Cube_cur};
            (Elem, Next_elem, Cube_cur) when Next_elem =/= none andalso
                                             Next_elem =/= visited ->
              Cube2 = rotate_right(Cube_cur),
              {visited, Cube2};
            (Elem, Next_elem, Cube_cur) ->
              Cube2 = rotate_down(Cube_cur),
              {visited, Cube2}
        end, Cube, List)), Acc}
    end, [], Grid)),
    lists: map(fun (Cubi) -> sum_arr(Cubi#cube.top) end, Cubics).

% ------------------------------------------------------------------------------
-spec mapfoldl_plus(Fun, In, list()) -> {list(), Out} when
      Fun :: fun((Elem, Next_elem, Cur) -> {Elem_up, Out}),
      Elem :: term(),
      Next_elem :: term() | none,
      Elem_up :: term(),
      In :: term(),
      Cur :: term(),
      Out :: term().

mapfoldl_plus(Fun, In, Arr) -> mapfoldl_plus(Fun, In, Arr, []).

mapfoldl_plus(_, Out, [], Acc) -> {lists: reverse(Acc), Out};
mapfoldl_plus(Fun, In, [H | T], Acc) ->
  Next_elem =
    case T of
      _ when length(T) =/= 0 -> hd(T);
      _ -> none
    end,
    {R, Out} = Fun(H, Next_elem, In),
  mapfoldl_plus(Fun, Out, T, [R | Acc]).

% ------------------------------------------------------------------------------
create_empty_arr(Start_cordinate, Finish_cordinate) ->
  [[_First_Elem | Rest_line] | Rest_grid] =
    [[[] || _X <- lists: seq(Start_cordinate#c.x, Finish_cordinate#c.x)]
       || _Y <- lists: seq(Start_cordinate#c.y, Finish_cordinate#c.y)],
  [[start | Rest_line] | Rest_grid].

% ------------------------------------------------------------------------------
sum_arr(Arr) ->
  lists: foldl(fun(X, S) -> X + S end, 0, Arr).
