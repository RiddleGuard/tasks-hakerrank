% https://www.hackerrank.com/challenges/fibonacci-fp/problem
-module(fibonachi).
-compile([export_all]).
-define(DBG(Z), (fun() -> io:format("~p~n", [Z]), Z end)()).

-export([main/0]).

main() ->
  {ok, [T]} = io:fread("","~d"), % 1 <= 10^4
  print_output(fibonachi(get_arr(T))).

% ------------------------------------------------------------------------------
get_arr(T) -> get_arr(T, []).

get_arr(0, Acc) -> Acc;
get_arr(T, Acc) ->
  {ok, [Num]} = io:fread("","~d"),
  get_arr(T - 1, [Num | Acc]).

% ------------------------------------------------------------------------------
print_output(List) ->
  lists:foreach(fun(L) ->
    io:fwrite("~p~n", [L])
  end, List).

% ------------------------------------------------------------------------------
-spec fibonachi(integer()) -> [integer()].

fibonachi(T) ->
  Cash = [],
  fibonachi(T, [], Cash).

fibonachi([], Acc, _) -> Acc;
fibonachi([H | T], Acc, Cash) ->
  {Res, Cash2} = fib(H, Cash),
  fibonachi(T, [Res rem 100000007 | Acc], Cash2).

% ------------------------------------------------------------------------------
fib(0, Cash) -> {0, Cash};
fib(1, Cash) -> {1, Cash};
fib(N, Cash) ->
  case lookup(N, Cash) of
    {error, not_found} ->
      {A, Cash2} = fib(N - 2, Cash),
      {B, Cash3} = fib(N - 1, Cash2),
      Res = A + B,
      case lists: keyfind(N, 1, Cash3) of
        false -> {Res, [{N, Res} | Cash3]};
        _ -> {Res, Cash3}
      end;
    Value -> {Value, Cash}
  end.

% ------------------------------------------------------------------------------
lookup(Name, Tab) ->
case lists: keyfind(Name, 1, Tab) of
    false ->
        {error, not_found};
    {Name, Value} ->
        Value
end.

% ------------------------------------------------------------------------------
-include_lib("eunit/include/eunit.hrl").

fib_test() ->
  Arr = [0, 1, 5, 10, 100],
  Res = [0, 1, 5, 55, 24278230],
  ?assertEqual(Res, lists: reverse(fibonachi(Arr))).
