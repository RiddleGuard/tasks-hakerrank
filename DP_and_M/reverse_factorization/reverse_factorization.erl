% https://www.hackerrank.com/challenges/reverse-factorization/problem
-module(reverse_factorization).
-compile([export_all]).
-define(DBG(Z), (fun() -> io:format("~p~n", [Z]), Z end)()).

-export([main/0]).
-define(SPACE, 32).
-define(CARRIAGE, 10).
-define(MAX_LENGTH_LINE, 20).

main() ->
  [S, _N | Arr] = read_all(),
  % print_output(condidate(?DBG(S), ?DBG(Arr))).
  print_output(condidate(S, Arr)).

% ------------------------------------------------------------------------------
read_all() ->
    read_all([], io: get_chars("", 4096)).
read_all(In, eof) ->
    lists: map(fun list_to_integer/1,
      string: tokens(lists:flatten(lists: reverse(In)), [?CARRIAGE, ?SPACE]));
read_all(In, Data) ->
    read_all([Data|In], io: get_chars("", 4096)).

% ------------------------------------------------------------------------------
print_output(List) ->
  lists:foreach(fun(Elem) -> io:fwrite("~p ", [Elem]) end, List).

% ------------------------------------------------------------------------------
% Определение:
% найти все варианты получения множителей для числа S;
% найти самую мальнькую последовательность(сти);
% найти среди них лексически наименьшее;
% ---
% мне нужно на каждой части проверять случаи со всеми возможными перемножениями
% 1. найти все дилителисреди доступных в массиве
% 2. Умножить temp ?= 1 на следующую цифру из Arr получится R:
%   Temp * hd(Arr) = R;
% 3. проверить результат с R == S;
% 4. если R > S, то [] ничего не найдено;
% 5. если R < S, то Temp = R (вернуться к 2)
condidate(S, Arr) ->
  Arr2 =
    lists: filter(fun(Elem) -> S rem Elem =:= 0 end, Arr),
  Lists =
    % condidate(S, 1, Arr2, [[]]),
  element(2, lists: foldl(fun (Elem, {Acc, Res}) ->
      Acc2 = [Elem | Acc],
      Res2 = min_factorization(condidate(S, 1, Acc2, [[]])),
      {Acc2, [Res2 | Res]}
    end, {[], []}, Arr2)),
  case min_factorization(Lists) of
    [] -> [-1];
    R -> R
  end.

condidate(S, Temp, Arr, Acc = [HAcc | TAcc]) ->
  Z =
    lists:map(fun (X) ->
      case Temp * X of
          S ->
            [lists: reverse([S, Temp | HAcc]) | TAcc];
          R when 0 < R andalso R < S ->
            condidate(S, R, Arr, [[Temp | HAcc] | TAcc]);
          R when 0 < R andalso R > S -> []
        end
    end, Arr),
  lists:append(Z).

% ------------------------------------------------------------------------------
-spec min_factorization([list()]) -> list().

min_factorization([]) -> [];
min_factorization(Lists) ->
  H = hd(Lists),
  element(1,
    lists: foldl(
      fun([], Acc) -> Acc;
        (List, {_, Len}) when length(List) < Len ->
          {List, length(List)};
        (List, {Res, Len}) when is_list(List)
                          andalso List =< Res
                          andalso length(List) =< Len ->
          {List, length(List)};
        (_, Acc) ->
          Acc
      end, {H, length(H)}, Lists)).
