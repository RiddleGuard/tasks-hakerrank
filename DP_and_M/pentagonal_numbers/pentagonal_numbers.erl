% https://www.hackerrank.com/challenges/pentagonal-numbers/problem
-module(pentagonal_numbers).
-compile([export_all]).
-define(DBG(Z), (fun() -> io:format("~p~n", [Z]), Z end)()).

-export([main/0]).
-define(TAB, cashe).
-define(TAB2, cashe_query).
-define(CARRIAGE, 10).

main() ->
  {ok, [_T]} = io: fread("","~d"),
  print_output(querys(read_all())).

querys(Arr) ->
  Pid = ets: new(?TAB, [set, duplicate_bag]),
  Pid2 = ets: new(?TAB2, [set, duplicate_bag]),
  lists: map(fun(E) ->
        case ets: lookup(Pid2, E) of
            [] -> R = p(E, Pid), ets:insert(Pid2, {E, R}), R;
            [{E, Value}] -> Value
        end
    end, Arr).

% ------------------------------------------------------------------------------
read_all() ->
    read_all([], io:get_chars("", 4096)).
read_all(In, eof) ->
    lists:map(fun list_to_integer/1,
      string:tokens(lists:flatten(lists:reverse(In)), [?CARRIAGE]));
read_all(In, Data) ->
    read_all([Data|In], io:get_chars("", 4096)).

% ------------------------------------------------------------------------------
print_output(List) ->
  lists: foreach(fun(L) ->
    io: fwrite("~p~n", [L])
  end, List).

p(1, _) -> 1;
p(2, _) -> 5;
p(N, Pid) -> p_c(N - 2, Pid) + 5 * N - 4.

% ------------------------------------------------------------------------------
p_c(1, _) -> 1;
p_c(N, Pid) ->
  case ets: lookup(Pid, N) of
    [] ->
      Res = N * 5 - (1 + 2 * N) + p_c(N - 1, Pid),
      ets: insert(Pid, {N, Res}),
      Res;
    [{N, Value}] ->
      Value
  end.

% ------------------------------------------------------------------------------
-include_lib("eunit/include/eunit.hrl").

rn_test() ->
  A = [1,  2, 3, 4, 5],
  Res = [1, 5, 12, 22, 35],
  ?assertEqual(Res, querys(A)).


rn2_test() ->
  A = [100000, 99999, 99998, 99997, 99996, 99995, 99994, 99993, 99992, 99991],
  Res = [14999950000, 14999650002, 14999350007, 14999050015, 14998750026,
         14998450040, 14998150057, 14997850077, 14997550100, 14997250126],
  ?assertEqual(Res, querys(A)).

