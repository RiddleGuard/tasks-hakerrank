#!/usr/bin/env bash
# author: (Riddle) Viktor Kondrahov
# date: 10/09/1018

erlc $1.erl
awk '
NR==FNR {a[NR]=$0; next; }
BEGIN {
	FS=":"
	i=0;
	printf("%s\t\t\t\t%s\t\t\t%s\n", "Your:", "Expected:", "Status:")
}
($0 in a) {
	if($0==a[FNR]) {
		i++;
		Status="Success"}
	else {
		Status="Fail"
	};
	printf("%s\t\t\t\t%s\t\t\t\t%s\n", $0, a[FNR], Status);
}
END {
	if(FNR==i) { printf("%s\n", "All Success") }
	else { printf("%s: %s\n", "There are failures", FNR-i) };
}
' output/output00.txt \
<(erl -noshell -s mirka main -s init stop < input/input00.txt)

## local
# awk -f ./../../parse_files.awk output/output00.txt \
# <(erl -noshell -s mirka main -s init stop < input/input00.txt)

## OLD
# time erl -noshell -s $1 main -s init stop < input/input$2.txt
# diff -y result.txt output/output$2.txt
