% https://www.hackerrank.com/challenges/valid-bst/problem
-module(valid_bsd).

-export([main/0]).

-define(SPACE, 32).
-define(NULL, -1).
-define(Y, "YES").
-define(N, "NO").
-define(MIN_COUNT_CASE, 1).
-define(MAX_COUNT_CASE, 10).
-define(MIN_COUNT_NODE, 1).
-define(MAX_COUNT_NODE, 100).

-record(tree, {value :: integer(),
               left  :: any(),
               rigth :: any()}).
-type tree() :: #tree{}.
% -type node(_V) :: tree() | ?NULL.

main() ->
  {ok, [X]} = io:fread("","~d"),
  List = read(X, []),
  print_output(List).

read([], Acc) -> Acc;
read(X, Acc) ->
  {ok, [Count_node]} = io: fread("","~d"),
  Line = io: get_line(""),
  Trees = parse(split_number(Line), Count_node, []),
  read(X - 1, [Trees | Acc]).

split_number(Line) -> split_number_int(string:to_integer(Line)).

split_number_int({Int, [?SPACE | Rest]}) -> [Int | split_number(Rest)];
split_number_int({Int, _})               -> [Int];
split_number_int(_)                      -> [].

print_output(List) ->
  lists:foreach(fun(Elem) -> io:fwrite("~p ", [Elem]) end, List).

parse([], 0, Acc) -> Acc;
parse([H, L, R | T], N, Acc) when L > R ->
  L2 = parse([R | T], N - 1, []),
  Tree = #tree{H, L2, L},
  parse(T, N - 1, [Tree | Acc]);
parse([H, L, R | T], N, Acc) when L < R ->
  parse(T, N - 1, [#tree{H, L, R} | Acc]).


% ------------------------------------------------------------------------------
% -ifdef(TEST).

% -include_lib("eunit/include/eunit.hrl").

% -endif.
