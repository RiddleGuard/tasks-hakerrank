% https://www.hackerrank.com/challenges/swap-nodes/problem
-module(binary_tree).
-compile([export_all]).

-export([main/0]).

-define(CARRIAGE, 10).
-define(ROOT_NODE, 1).
-define(NULL, -1).

-type tree(Value, Left, Rigth) :: {Value, Left, Rigth}.
-type tree() :: tree(integer(), tree(), tree()).

main() ->
  List = read_input(),
  N = hd(hd(List)),
  {BTA, TA} =
    lists:foldr(fun ([Left, Rigth], {BTreeArr, TreeArr}) ->
      {[Left | [ Rigth | BTreeArr]], TreeArr};
                    (Tree, {BTreeArr, TreeArr}) ->
      {BTreeArr, [Tree | TreeArr]}
    end, {[], []}, tl(List)),
  % Start_h = list_to_binary(hd(tl(TA))),
  % swap_operation(lists:reverse(BTA), N, Start_h)
  T = parser([N | BTA], list_to_integer(N)),
  print_output([T]).

read_input() ->
  read_input(io:get_line(""), []).

read_input(eof, Acc) ->
  lists:reverse(Acc);
read_input(Data, Acc) ->
  read_input(io:get_line(""), [spliter(Data) | Acc]).

spliter(Line) ->
  string: split(string:strip(Line, right, ?CARRIAGE), [32], all).

print_output(List) ->
  lists:foreach(fun(Elem) -> io:fwrite("~p~n", [Elem]) end, List).


% ------------------------------------------------------------------------------
-spec parser(list(), integer()) -> tree().

parser(List, CountS, CountE, 1) ->
  {hd(List), nil, nil};
parser(List, CountS, CountE, N) ->
  case {hd(tl(List)), hd(tl(tl(List)))} of
    {-1, -1} -> ok; %0
    {-1, _} -> ok; %1
    {_, -1} -> ok; %1
    {_, _} ->  ok %2
  end,
  % {hd(List), parser([hd(tl(List)) | tl(tl(tl(List))), N - 1), parser(tl(tl(List)), N - 1)}.
  {hd(List), parser([hd(tl(List)) | tl(tl(tl(List)))], CountS, CountE, N - 1),
             parser(tl(tl(List)), CountS, CountE, N - 1)}.

swap3([{-1, -1}]) ->
  [{-1, -1}];
swap3([{L, -1}, _T]) ->
  [{-1, L}, {-1, -1}];
swap3([{-1, R}, T]) ->
  [{R, -1}, T];
swap3([{L, R}, SL, SR]) ->
  [{R, L}, SR, SL].

% ------------------------------------------------------------------------------
-include_lib("eunit/include/eunit.hrl").

swap_operation_test() ->
  BTA = [{2, 3},
         {-1, -1}, {-1, -1}],
  Res = [{3, 2},
         {-1, -1}, {-1, -1}],
  N = 3, % count depth
  H = 2, % depth
  ?assertEqual(Res, swap3(BTA)).


swap_operation2_test() ->
  BTA = [{2, 3},
         {7, 5}, {4, 6},
         {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}],
  Res = [{3, 2},
         {4, 6}, {7, 5},
         {-1, -1}, {-1, -1}, {-1, -1}, {-1, -1}],
  N = 4, % count depth
  H = 2, % depth
  ?assertEqual(Res, swap3(BTA)).

swap_operation3_test() ->
  BTA = [{2, 3},
         {-1, 5}, {4, -1},
         {-1, -1}, {-1, -1}],
  Res = [{3, 2},
         {4, -1}, {-1, 5},
         {-1, -1}, {-1, -1}],
  N = 4, % count depth
  H = 2, % depth
  ?assertEqual(Res, swap3(BTA)).

swap_operation4_test() ->
  BTA = [{2, 3},
         {3, -1}, {-1, 6},
         {-1, 7}, {2, -1},
         {-1, -1}, {2, -1},
         {-1, -1}],
  Res = [{3, 2},
         {-1, 6}, {3, -1},
         {2, -1}, {-1, 7},
         {2, -1}, {-1, -1},
         {-1, -1}],
  N = 6, % count depth
  H = 2, % depth
  ?assertEqual(Res, swap3(BTA)).

swap_operation5_test() ->
  BTA = [{3, 2},
         {-1, 6}, {3, 4},
         {2, -1}, {-1, 7}, {-1, 2},
         {2, -1}, {-1, -1}, {-1, 2},
         {-1, -1}, {-1, -1}],
  Res = [{2, 3},
         {3, 4}, {-1, 6},
        % 2 ^ (4 -1) - 2 -1 -1 -1 = 3 элементов,
        % где (-2) отсутствие целого элемента, a 4 это глубина (depth)
         {-1, 7}, {-1, 2}, {2, -1},
        % 2 ^ (5 -1) - 2 - 2 -1 -1 -1 -6 = 3
         {-1, -1}, {-1, 2}, {2, -1},
         {-1, -1}, {-1, -1}],
  N = 6, % count depth
  H = 2, % depth
  ?assertEqual(Res, swap3(BTA)).

conversion_test() ->
  BTA = [{3, 2},
         {-1, 6}, {3, 4},
         {2, -1}, {-1, 7}, {-1, 2},
         {2, -1}, {-1, -1}, {-1, 2},
         {-1, -1}, {-1, -1}],
  Res = "3 2 2 6 1 3 7 2 4 2 2",
  N = 6, % count depth
  H = 2, % depth
  % ?assertEqual("3 2 2 6 1 3 7 2 4 2 2", conversion(BTA)),
  ok.

conversion2_test() ->
  BTA = [{2, 3},
         {-1, 4}, {-1, 5},
         {6, -1}, {7, 8},
         {-1, 9}, {-1, -1}, {10, 11},
         {-1, -1}, {-1, -1}, {-1, -1}],
  Res = "2 6 9 4 1 3 7 5 10 8 11",
  N = 6, % count depth
  H = 2, % depth
  % ?assertEqual("3 2 2 6 1 3 7 2 4 2 2", conversion(BTA)),
  ok.
