% Enter your code here. Read input from STDIN. Print output to STDOUT
% Your class should be named solution
-compile([export_all]).
-module(mirka).

% N - Count houses
% Q - Count querys
% T - Index heights houses
% SE - Start Element
% ST - Step Element

-export([main/0]).
% кол-во этажей - T
-define(MIN, 1).
-define(MAX, 100000).

% кол-во запросов - Q
-define(MIN_QUERY, 1).
-define(MAX_QUERY, 100000).

% кол-во домов по индексам - N
-define(MIN_INDEX, 1).
-define(MAX_INDEX, 100000).

-define(SPACE, 32).
-define(CARRIAGE, 10).

main() ->
  [N, _Qurys, SD, Days] = parse(read_input()),
  %SD1 = lists: sort(fun ({_, B1, _}, {_, B2, _}) -> B2 =< B1 end, SD),
  print_output(lists: map(fun (Day) ->
      get_arr_heights(SD, Day + 1, N)
    end, Days)).

% ------------------------------------------------------------------------------
-spec parse([list()]) -> [list()].

parse(Data) ->
  [N_Qurys, Start_heights, Step_heights | Days] = Data,
  [N, Qurys] = split(N_Qurys),
  SD = lists: zip3(lists: seq(1, N), split(Start_heights), split(Step_heights)),
  [N, Qurys, SD, ll2li(Days)].

% ------------------------------------------------------------------------------
-spec split(string()) -> [integer()].

split(Str) ->
  ll2li(string: split(Str, [?SPACE], all)).

% ------------------------------------------------------------------------------
-spec read_input() -> [list()].

read_input() ->
  read_input(io: get_line(""), []).

read_input(eof, Acc) ->
  lists: reverse(Acc);
read_input(Data, Acc) ->
  read_input(io: get_line(""), [spliter(Data) | Acc]).

% ------------------------------------------------------------------------------
-spec spliter(list()) -> list().

spliter(Line) ->
  string: strip(Line, right, ?CARRIAGE).

% ------------------------------------------------------------------------------
-spec print_output(list()) -> ok.

print_output(List) ->
  lists: foreach(fun(Elem) -> io: fwrite("~p~n", [Elem]) end, List).

% ------------------------------------------------------------------------------
-spec ll2li([list()]) -> [integer()].

ll2li(LL) -> lists: map(fun erlang: list_to_integer/1, LL).

% ------------------------------------------------------------------------------
-spec max_of_step(List) -> Max when
      List :: [T,...],
      Max :: {integer(), integer(), integer()},
      T :: term().

max_of_step([H | T]) -> max_of_step(T, H).

max_of_step([{_, _, S} = Elem | T], {_, _, S2}) when S > S2 ->
  max_of_step(T, Elem);
max_of_step([_ | T], Max) -> max_of_step(T, Max);
max_of_step([], Max) -> Max.

% ------------------------------------------------------------------------------
-spec max_of_start_elem(List) -> Max when
      List :: [T,...],
      Max :: {integer(), integer(), integer()},
      T :: term().

max_of_start_elem([H | T]) -> max_of_start_elem(T, H).

max_of_start_elem([{_, S, _} = Elem | T], {_, S2, _}) when S > S2 ->
  max_of_start_elem(T, Elem);
max_of_start_elem([_ | T], Max) -> max_of_start_elem(T, Max);
max_of_start_elem([], Max) -> Max.

% ------------------------------------------------------------------------------
-spec get_iter(list()) -> {integer(), integer()}.

get_iter(SD) ->
  {IndexElemMax, StartElemMax, StepElemMax} = max_of_step(SD),
  {_IndexElemStart, StartElemStart, _StepElemStart} = max_of_start_elem(SD),
  {StartElemStart - StartElemMax / StepElemMax, IndexElemMax}.

% ------------------------------------------------------------------------------
get_arr_heights(SD, Day, N) ->
  {CountDays, Index} = get_iter(SD),
  % io:format("~p :: ~p~n",[Day, {CountDays, Index}]),
  case Day > CountDays of
    true ->
      io:format("ewfdew"),
      Index;
    false -> max_index(get_arr_heights(SD, Day, N, []))
  end.

get_arr_heights(_, _, 0, Acc) ->
  lists:reverse(Acc);
get_arr_heights([{_IndexElem, SE, StE} | SD], Day, Index, Acc) ->
  get_arr_heights(SD, Day, Index - 1, [get_query(SE, StE, Day) | Acc]).

% ------------------------------------------------------------------------------
% функция арифметической прогрессии.
-spec get_query(Start_elem, Step, Day) -> Res when
      Start_elem :: ?MIN..?MAX, % стартовое кол-во этажей
      Step :: integer(),
      Day :: integer(),
      Res :: ?MIN..?MAX. % кол-во этажей в день Day

get_query(Start_elem, Step, Day) ->
  Start_elem + (Day - 1) * Step.

% ------------------------------------------------------------------------------
-spec max_index(Arr, {I, Max}, Index) -> MaxIndex when
      Max :: ?MIN..?MAX,
      I :: integer(),
      Index :: integer(),
      MaxIndex :: integer(),
      Arr :: ?MIN_INDEX..?MAX_INDEX.

max_index([H|T]) -> max_index([H|T], {1, H}, 1).

max_index([H|T], {_I, Max}, Index) when H >= Max ->
  max_index(T, {Index, H}, Index + 1);
max_index([_|T], {I, Max}, Index) ->
  max_index(T, {I, Max}, Index + 1);
max_index([],{I, _}, _) -> I.
