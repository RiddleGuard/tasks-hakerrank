% https://www.hackerrank.com/challenges/functional-programming-warmups-in-recursion---gcd/problem
-module(string_mingling).
-compile([export_all]).

-export([main/0]).
-define(CARRIAGE, 10).

main() ->
  Str1 = string: strip(io:get_line(""), right, ?CARRIAGE),
  Str2 = string: strip(io:get_line(""), right, ?CARRIAGE),
  io:fwrite("~s~n", [string_mingling(Str1, Str2)]).

-spec string_mingling(list(), list()) -> list().

string_mingling(Str1, Str2) when length(Str1) =:= length(Str2) ->
  string_mingling(Str1, Str2, []).

string_mingling([], [], Acc) -> lists:reverse(Acc);
string_mingling([H1 | T1], [H2 | T2], Acc) ->
  string_mingling(T1, T2, [H2, H1 | Acc]).

% ------------------------------------------------------------------------------
-include_lib("eunit/include/eunit.hrl").

string_mingling_test() ->
  Str1 = "hacker",
  Str2 = "ranker",
  Res  = "hraacnkkeerr",
  ?assertEqual(Res, string_mingling(Str1, Str2)).
