-module(concave_polygon).
-compile([export_all]).
-define(DBG(Z), (fun() -> io:format("~p~n", [Z]), Z end)()).

-export([main/0]).
-define(SPACE, 32).
-define(CARRIAGE, 10).
-define(NO, "NO").
-define(YES, "YES").

main() ->
  Arr = parse(tl(read_all())),
  io:format(breaker(concave_polygon(hd(Arr) ,Arr))).

% ------------------------------------------------------------------------------
-spec parse(list()) -> [{integer(), integer()}].

parse(Arr) -> parse(Arr, []).

parse([], Acc) -> [hd(Acc) | lists: reverse(Acc)];
parse([X, Y | Arr], Acc) ->
  parse(Arr, [{list_to_integer(X), list_to_integer(Y)} | Acc]).

% ------------------------------------------------------------------------------
-spec read_all() -> list().

read_all() ->
    read_all([], io: get_chars("", 4096)).
read_all(In, eof) ->
    string: tokens(lists:flatten(lists: reverse(In)), [?CARRIAGE, ?SPACE]);
read_all(In, Data) ->
    read_all([Data|In], io: get_chars("", 4096)).

% ------------------------------------------------------------------------------
-spec concave_polygon(tuple(), list()) -> boolean().

concave_polygon(Start, Arr = [{X1, Y1}, {X2, Y2}, {Xx, Yx}])
  when length(Arr) =:= 3 ->
  not equation_line_right(Start, {X1, Y1}, {X2, Y2}, {Xx, Yx});
concave_polygon(Start, [{X1, Y1}, {X2, Y2}, {X3, Y3} | Tail]) ->
  case equation_line_right(Start, {X1, Y1}, {X2, Y2}, {X3, Y3}) of
    true -> concave_polygon(Start, [{X2, Y2}, {X3, Y3} | Tail]);
    _ -> false
  end.

% ------------------------------------------------------------------------------
-spec equation_line_right(A, A, B, C) -> boolean() when
      A :: {integer(), integer()},
      B :: {integer(), integer()},
      C :: {integer(), integer()}.

equation_line_right({_StartX, StartY}, {X1, Y1}, {X2, Y2}, {Xx, Yx}) ->
  FunY =  fun(_) when X2 - X1 =:= 0 -> Y1;
            (X) -> (((X - X1) / (X2 - X1)) * (Y2 - Y1)) + Y1
          end,
  ?DBG(lists:duplicate(30, $-)),
  ?DBG(["{X1, Y1} = ", {X1, Y1}]),
  ?DBG(["{X2, Y2} = ", {X2, Y2}]),
  ?DBG(["{Xx, Yx} = ", {Xx, Yx}]),
  ?DBG(["FunY(Xx) = ", FunY(Xx), "Yx =", Yx, "Xx =", Xx, Yx =< FunY(Xx)]),
  ?DBG("FunY(Xx) >= Yx"),
  FunY(Xx) >= StartY.

% ------------------------------------------------------------------------------
-spec breaker(boolean()) -> any().

breaker(true) -> ?YES;
breaker(false) -> ?NO.

% ------------------------------------------------------------------------------
rotate({X1, Y1}, {X2, Y2}, {X3, Y3}) ->
  (X2 - X1) * (Y3 - Y1) - (Y2 - Y1) * (X3 - X1).

