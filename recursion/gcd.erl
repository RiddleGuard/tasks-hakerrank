% https://www.hackerrank.com/challenges/lists-and-gcd/problem
-module(gcd).

-export([main/0]).

-define(CARRIAGE, 10).
-define(SPACE, 32).

main() ->
  List = read_input(),
  List_num = lists: foldl(fun (Sub_list, Acc) ->
      Sub_list2 = string: split(Sub_list, [?SPACE], all),
      [lists: map(fun list_to_integer/1, Sub_list2) | Acc]
    end, [], tl(List)),
  print_output(multi_gcd(List_num)).

read_input() ->
  read_input(io:get_line(""), []).

read_input(eof, Acc) ->
  lists:reverse(Acc);
read_input(Data, Acc) ->
  read_input(io:get_line(""), [spliter(Data) | Acc]).

spliter(Line) ->
  string:strip(Line, right, ?CARRIAGE).

print_output(List) ->
  lists:foreach(fun(Elem) -> io:fwrite("~p ", [Elem]) end, List).

% ------------------------------------------------------------------------------
multi_gcd([B]) -> B;
multi_gcd(List) ->
  gcd(multi_gcd(tl(List)), hd(List), []).

gcd([], _B, Acc) ->
  lists:flatten(Acc);
gcd([A, DegreeA | T], B, Acc) ->
  gcd(T, B, Acc ++ gcd_right({A, DegreeA}, B, []) ).

% возможно возвращает только одно значение
gcd_right(_, [], Acc) ->
  Acc;
gcd_right({Number, Degree}, [B, DegreeB | T], Acc) ->
  case B of
    Number ->
      [Number, minim(Degree, DegreeB)];
    _ ->
      gcd_right({Number, Degree}, T, Acc)
  end.

% ------------------------------------------------------------------------------
% min
minim(A, B) when A > B -> B;
minim(A, _) -> A.

% ------------------------------------------------------------------------------
% -ifdef(TEST).

-include_lib("eunit/include/eunit.hrl").

gcd_test() ->
  A = [2, 3, 7, 4, 5, 2],
  B = [2, 4, 5, 2],
  Res = [2, 3, 5, 2],
  ?assertEqual(Res, gcd(A, B, [])).

gcd2_test() ->
  A = [3, 2, 5, 3, 11, 1],
  B = [2, 2, 3, 3, 5, 4, 7, 6, 19, 18],
  Res = [3, 2, 5, 3],
  ?assertEqual(Res, gcd(A, B, [])).

multi_gcd_test() ->
  A = [2, 7, 3, 2, 5, 3, 11, 1],
  B = [2, 2, 3, 3, 5, 4, 7, 6, 19, 18],
  C = [2, 3, 5, 2, 7, 4],
  Res = [2, 2, 5, 2],
  ?assertEqual(Res, multi_gcd([A, B, C])).

multi_gcd2_test() ->
  A = [2, 2, 3, 2, 5, 3],
  B = [3, 2, 5, 3, 11, 1],
  C = [2, 2, 3, 3, 5, 4, 7, 6, 19, 18],
  D = [3, 10, 5, 15],
  Res = [3, 2, 5, 3],
  ?assertEqual(Res, multi_gcd([A, B, C, D])).

multi_gcd3_test() ->
  A =
    [[2, 5, 3, 1, 5, 4, 7, 3, 13, 1, 19, 1, 23, 2, 31, 1],
    [2, 3, 3, 2, 5, 4, 7, 2, 29, 1],
    [2, 5, 3, 1, 5, 4, 7, 2, 11, 3, 13, 1],
    [2, 4, 3, 1, 5, 5, 7, 3, 19, 1, 23, 1, 31, 2],
    [2, 4, 3, 4, 5, 4, 7, 2, 13, 1, 23, 1, 29, 1],
    [2, 3, 3, 1, 5, 4, 7, 4, 11, 1, 19, 1],
    [2, 4, 3, 1, 5, 4, 7, 2],
    [2, 5, 3, 1, 5, 5, 7, 2, 13, 2, 19, 1, 29, 1],
    [2, 3, 3, 1, 5, 6, 7, 2],
    [2, 3, 3, 2, 5, 4, 7, 3, 17, 1, 31, 1]],
  Res = [2, 3, 3, 1, 5, 4, 7, 2],
  ?assertEqual(Res, multi_gcd(A)).

% -endif.
