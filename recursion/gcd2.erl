% https://www.hackerrank.com/challenges/functional-programming-warmups-in-recursion---gcd/problem
-module(gcd2).
-compile([export_all]).

-export([main/0]).

-define(CARRIAGE, 10).
-define(SPACE, 32).

main() ->
  [A, B] = lists:map(fun list_to_integer/1, string: split(string: strip(
    io: get_line(""), right, ?CARRIAGE), [?SPACE], all)),
  io:fwrite("~p~n", [gcd(A, B)]).

gcd(A, B)  ->
  case A =/= B of
    true ->
      {A2, B2} =
        case A > B of
          true -> {A - B, B};
          false -> {A, B - A}
        end,
      gcd(A2, B2);
    false ->
      A
  end.
