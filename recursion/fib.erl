% https://www.hackerrank.com/challenges/functional-programming-warmups-in-recursion---fibonacci-numbers/problem
-module(fib).
-compile([export_all]).

-export([main/0]).

-define(MAX_LENGTH, 40).

main() ->
  {ok, [N]} = io:fread("","~d"),
  io: fwrite("~p~n", [fib(N)]).

fib(N) when 0 < N andalso N =< ?MAX_LENGTH ->
  case N of
    1 -> 0;
    2 -> 1;
    _ when N > 2 -> fib(N - 1) + fib(N - 2)
  end.

fib2(N) -> fib2(N, []).

fib2(0, Acc) -> Acc;
fib2(N, Acc) -> fib2(N - 1, [fib(N) | Acc]).
