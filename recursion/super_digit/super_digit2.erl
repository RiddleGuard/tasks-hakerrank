-module(super_digit2).

-export([main/0]).

super_digit(R) when R==0 -> 9;
super_digit(R) -> R.
main() ->
    math:pow(X, Y)
    {ok,[First,Second]}=io:fread("","~d~d"),
    io:format("~w~n",[super_digit((First*Second) rem 9)]).
