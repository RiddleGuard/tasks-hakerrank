-module(super_digit).
-compile([export_all]).

-export([main/0]).

main() ->
  {ok, [A, B]} = io:fread("", "~s~d"),
  Arr =
    case B of
      1 -> list_to_integer(A);
      B -> R = super_digit(A),
          super_digit(integer_to_list(R * B))
    end,
  print_output([Arr]).

% ------------------------------------------------------------------------------
print_output(List) ->
  lists:foreach(fun(Elem) -> io:fwrite("~p", [Elem]) end, List).

% ------------------------------------------------------------------------------
super_digit(Arr) -> super_digit(Arr, 0).

super_digit([], Acc) when Acc div 10 =:= 0 -> Acc;
super_digit([], Acc) -> super_digit(integer_to_list(Acc), 0);
super_digit([H | T], Acc) ->
  super_digit(T, list_to_integer([H]) + Acc).
