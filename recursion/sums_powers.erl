% https://www.hackerrank.com/challenges/functional-programming-the-sums-of-powers/problem

-module(sums_powers).
-compile([export_all]).
-export([main/0]).

main() ->
  {ok, [X]} = io:fread("","~d"),
  {ok, [Y]} = io:fread("","~d"),
  io:fwrite("~p~n", [nWays(X, Y, 1)]).

nWays(X, N) ->
  nWays(X, N, 1).

nWays(X, N, Num) ->
  case trunc(math: pow(Num, N)) of
    R when R < X   -> nWays(X, N, Num + 1) + nWays(X - R, N, Num + 1);
    R when R =:= X -> 1;
    R when R > X   -> 0
  end.

% ------------------------------------------------------------------------------

-include_lib("eunit/include/eunit.hrl").


sums_powers_1_test() ->
  X = 30,
  A = nWays(X, 2, 1),
  Res = 2,
  ?assertEqual(Res, A).

sums_powers5_test() ->
  A = nWays(30, 2),
  Res = 2,
  ?assertEqual(Res, A).

sums_powers6_test() ->
  A = nWays(100, 2),
  Res = 3,
  ?assertEqual(Res, A).

sums_powers7_test() ->
  A = nWays(100, 3),
  Res = 1,
  ?assertEqual(Res, A).

sums_powers8_test() ->
  A = nWays(800, 2),
  Res = 561,
  ?assertEqual(Res, A).

find_sum_s2_test() ->
  A = nWays(100, 2),
  Res = length([[100], [36, 64], [1, 9, 16, 25, 49]]),
  ?assertEqual(Res, A).
