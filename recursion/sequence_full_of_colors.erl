% https://www.hackerrank.com/challenges/sequence-full-of-colors/problem
-module(sequence_full_of_colors).
-export([main/0]).
-define(RED,     [82]).
-define(GREEN,   [71]).
-define(BLUE,    [66]).
-define(YELLOW,  [89]).
-define(MIN_LEN, 1).
-define(MAX_LEN, 10).
-define(CARRIAGE, 10).

-type colors() :: ?RED | ?GREEN | ?BLUE | ?YELLOW.

main() ->
  {ok, [Count_case]} = io: fread("","~d"),
  Lists = get_line(Count_case),
  print_output(check_rule(Lists, Count_case)).

% ------------------------------------------------------------------------------
print_output(List) ->
  lists:foreach(fun(L) when is_boolean(L) ->
    io:fwrite("~s~n", [pascal_case(erlang:atom_to_list(L))])
  end, List).

% ------------------------------------------------------------------------------
get_line(N) -> get_line(N, []).

get_line(0, Acc) -> lists:reverse(Acc);
get_line(N, Acc) ->
  Line = string: strip(io: get_line(""), right, ?CARRIAGE),
  get_line(N - 1, [Line | Acc]).

% ------------------------------------------------------------------------------
check_rule(List, N) when ?MIN_LEN =< N andalso N =< ?MAX_LEN ->
  lists: reverse(
    lists:foldl(
      fun(Arr, Acc) ->
        [check_rule2(Arr) | Acc]
      end,
    [], List)).

% ------------------------------------------------------------------------------
-spec check_rule2([colors()]) -> boolean().

check_rule2(Arr) -> check_rule2(Arr, {0, 0}).

check_rule2([], {SB, SY}) ->
  SB =:= 0 andalso SY =:= 0;
check_rule2([H | T], {SB, SY}) ->
  case H of
    H when [H] =:= ?RED    -> SB =< 1 andalso check_rule2(T, {SB + 1, SY});
    H when [H] =:= ?GREEN  -> SB =< 1 andalso check_rule2(T, {SB - 1, SY});
    H when [H] =:= ?YELLOW -> SY =< 1 andalso check_rule2(T, {SB, SY + 1});
    H when [H] =:= ?BLUE   -> SY =< 1 andalso check_rule2(T, {SB, SY - 1});
    _ -> false
  end.

% ------------------------------------------------------------------------------
pascal_case([H | T]) ->
  [string: to_upper(H) | T].

% ------------------------------------------------------------------------------
-include_lib("eunit/include/eunit.hrl").

r_test() ->
  N = 4,
  A = ["RGGR", "RYBG", "RYRB", "YGYGRBRB"],
  Res = [true, true, false, false],
  ?assertEqual(Res, check_rule(A, N)).
