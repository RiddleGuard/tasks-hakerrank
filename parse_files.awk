#!/usr/bin/env awk -f

NR==FNR {a[NR]=$0; next; }
BEGIN {
	FS=":"
	i=0;
	printf("%s\t\t\t\t%s\t\t\t%s\n", "Your:", "Expected:", "Status:")
}
($0 in a) {
	if($0==a[FNR]) {
		i++;
		Status="Success"}
	else {
		Status="Fail"
	};
	printf("%s\t\t\t\t%s\t\t\t\t%s\n", $0, a[FNR], Status);
}
END {
	if(FNR==i) { printf("%s\n", "All Success") }
	else { printf("%s: %s\n", "There are failures", FNR-i) };
}
